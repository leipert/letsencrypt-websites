# Automatically update Let's Encrypt certificates

This repository updates the Let's Encrypt certificates for https://leipert.io and https://no-thank-you.de automatically.

Both are hosted on GitLab Pages. To update the certificate we utilize GitLab CI:

1. Run a [scheduled job](https://gitlab.com/leipert/letsencrypt-websites/pipeline_schedules) which checks if the certificate expires within 30 days.
2. If yes, add a ACME challenge to the source repo, wait for the page to be published and LE to issue an certificate
3. Update the certificate via the API
4. Delete the ACME challenge files again

The config is rather simple: https://gitlab.com/leipert/letsencrypt-websites/blob/master/.gitlab-ci.yml

For detailed installation instructions have a look over at: https://gitlab.com/leipert-projects/gitlab-letsencrypt#usage-with-gitlab-ci
